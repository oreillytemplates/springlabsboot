<%@ include file="/WEB-INF/layouts/include.jsp"%>

<h1>Add New Car Part</h1>
<div class="row">
	<div class="col-sm-12">
		<form is="orly-form" id="form" method="post" action="<c:url value='/carparts/partnumber' />">
		<%-- <form is="orly-form" id="form" method="post" action="${pageContext.request.contextPath}/carparts/partnumber"> --%>
			<c:if test="${not empty carpart.partNumber}">
			    <input type="hidden" id="update" name="update" value="true" />
			</c:if>
			<div class="col-sm-4 form-group">
			  <label for="partnumber">Part #</label>
			  <orly-input id="partnumber" name="partnumber" value="${carpart.partNumber}" placeholder="Part #"></orly-input>
			</div>
			<div class="col-sm-4 form-group">
			  <label for="title">Title</label>
			  <orly-input id="title" name="title" value="${carpart.title}"  placeholder="Title"></orly-input>
			</div>
			<div class="col-sm-4 form-group">
			  <!-- <orly-btn id="submitBtn" type="submit" spinonclick text="Submit"></orly-btn> -->
			  <button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>
		<div>
			<c:if test="${not empty carpart.partNumber}">
			    <b>Part Number:</b> ${carpart.partNumber}<br/>
			    <b>Title:</b> ${carpart.title}<br/>
			    <a href="<c:url value='/carparts/delete/${carpart.partNumber}'/>" class="btn btn-danger">Delete</a>
			</c:if>
		</div>
	</div>
</div>
