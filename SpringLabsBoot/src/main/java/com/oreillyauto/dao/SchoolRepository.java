package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.SchoolRepositoryCustom;
import com.oreillyauto.domain.schools.School;

public interface SchoolRepository extends CrudRepository<School, Integer>, SchoolRepositoryCustom {
    
}
