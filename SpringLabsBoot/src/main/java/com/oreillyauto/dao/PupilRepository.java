package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.PupilRepositoryCustom;
import com.oreillyauto.domain.foo.Pupil;

public interface PupilRepository extends CrudRepository<Pupil, Integer>, PupilRepositoryCustom {
        
}
