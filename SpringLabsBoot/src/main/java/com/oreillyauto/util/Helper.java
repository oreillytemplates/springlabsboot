package com.oreillyauto.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;


public class Helper {
	
    public static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    
	public static String getJson(List<?> list) throws JsonGenerationException, JsonMappingException, IOException {
	    final ByteArrayOutputStream out = new ByteArrayOutputStream();
	    final ObjectMapper mapper = new ObjectMapper();
	    mapper.writeValue(out, list);
	    final byte[] data = out.toByteArray();
	    return new String(data);
	}

	public static String getJson(Object obj) throws JsonProcessingException {
		if (obj == null) {
			return new JsonObject().toString();
		}
		
		return new ObjectMapper().writeValueAsString(obj);
	}

	public static boolean hasItems(List<?> list) {
		if (list != null && list.size() > 0) {
			return true;
		}
		
		return false;
	}

	public static void printList(List<?> list) {
		if (list != null && list.size() > 0) {
			for (Object object : list) {
				System.out.println(object);
			}
		}
	}

	public static String getError(Exception e) {
		return ((e == null) ? "Null Pointer Exception" : e.getMessage());
	}
}
