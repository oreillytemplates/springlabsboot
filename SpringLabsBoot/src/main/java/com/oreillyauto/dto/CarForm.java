package com.oreillyauto.dto;

import java.io.Serializable;

public class CarForm implements Serializable {

    private static final long serialVersionUID = -6587431151280708554L;

    public CarForm() {}

    public CarForm(String partNumber, String title) {
        this.partNumber = partNumber;
        this.title = title;
    }
    
    private String partNumber;
    private String line;
    private String title;
    private String description;
    private String imageName;
    private String cyclinders;
    private Boolean error;
    private String errorMessage;
    
    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getCyclinders() {
        return cyclinders;
    }

    public void setCyclinders(String cyclinders) {
        this.cyclinders = cyclinders;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
