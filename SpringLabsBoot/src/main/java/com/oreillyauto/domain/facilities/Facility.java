package com.oreillyauto.domain.facilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="FACILITIES")
public class Facility implements Serializable {
	private static final long serialVersionUID = -5996834027120348470L;
    public Facility() {}
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "facility_id", columnDefinition = "INTEGER")
    private Integer facilityId;

    @Column(name = "facility_name", columnDefinition = "VARCHAR(64)")
	private String facilityName;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "facility")
    private List<Teammember> teammemberList = new ArrayList<Teammember>();
    
	public Integer getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(Integer facilityId) {
		this.facilityId = facilityId;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public List<Teammember> getTeammemberList() {
		return teammemberList;
	}

	public void setTeammemberList(List<Teammember> teammemberList) {
		this.teammemberList = teammemberList;
	}

	@Override
	public String toString() {
		return "Facility [facilityId=" + facilityId + ", facilityName=" + facilityName + "]";
	}
        
}
