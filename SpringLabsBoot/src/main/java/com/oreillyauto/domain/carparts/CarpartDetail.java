package com.oreillyauto.domain.carparts;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

//@Immutable
@Table(name = "CARPARTDETAILS")
@Entity
public class CarpartDetail implements Serializable {
    
    private static final long serialVersionUID = 2179090054779583164L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DETAIL_ID", columnDefinition = "INTEGER")
    private Integer detailId;
    
    @Column(name = "DETAIL_KEY", columnDefinition = "VARCHAR(256)")
    private String detailKey;
    
    @Column(name = "DETAIL_VALUE", columnDefinition = "VARCHAR(256)")
    private String detailValue;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PART_NUMBER", referencedColumnName = "PART_NUMBER", columnDefinition = "VARCHAR(64)")
    private Carpart carpart;

    public Integer getDetailId() {
        return detailId;
    }

    public void setDetailId(Integer detailId) {
        this.detailId = detailId;
    }

    public String getDetailKey() {
        return detailKey;
    }

    public void setDetailKey(String detailKey) {
        this.detailKey = detailKey;
    }

    public String getDetailValue() {
        return detailValue;
    }

    public void setDetailValue(String detailValue) {
        this.detailValue = detailValue;
    }

    public Carpart getCarpart() {
        return carpart;
    }

    public void setCarpart(Carpart carpart) {
        this.carpart = carpart;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((carpart == null) ? 0 : carpart.hashCode());
        result = prime * result + ((detailId == null) ? 0 : detailId.hashCode());
        result = prime * result + ((detailKey == null) ? 0 : detailKey.hashCode());
        result = prime * result + ((detailValue == null) ? 0 : detailValue.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CarpartDetail other = (CarpartDetail) obj;
        if (carpart == null) {
            if (other.carpart != null)
                return false;
        } else if (!carpart.equals(other.carpart))
            return false;
        if (detailId == null) {
            if (other.detailId != null)
                return false;
        } else if (!detailId.equals(other.detailId))
            return false;
        if (detailKey == null) {
            if (other.detailKey != null)
                return false;
        } else if (!detailKey.equals(other.detailKey))
            return false;
        if (detailValue == null) {
            if (other.detailValue != null)
                return false;
        } else if (!detailValue.equals(other.detailValue))
            return false;
        return true;
    }

    // BAD TO STRING METHOD (Hold reference to carpart and carpart may hold reference to carpartdetail!
    // This can lead to a circular reference and an exception!
//    @Override
//    public String toString() {
//        return "CarpartDetail [detailId=" + detailId + ", detailKey=" + detailKey + ", detailValue=" + detailValue + ", "
//                + "carpart=" + carpart + "]";
//    }
    
    // Good TO STRING METHOD
    @Override
    public String toString() {
        return "CarpartDetail [detailId=" + detailId + ", detailKey=" + detailKey + ", detailValue="
                + detailValue + "]";
    }  

}
