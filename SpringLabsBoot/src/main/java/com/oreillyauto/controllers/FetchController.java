package com.oreillyauto.controllers;

import org.apache.http.HttpResponse;
import org.apache.http.protocol.HTTP;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FetchController extends BaseController {
	
	@GetMapping(value = "/fetch")
	public String getFetch() {
		return "fetch";
	}
    
    @GetMapping(value = "/fetch/404")
    public String getFetch(HttpResponse response) {
        
        if (1==1)  {
            response.setStatusCode(404);
        }
        
        return "fetch";
    }
}
