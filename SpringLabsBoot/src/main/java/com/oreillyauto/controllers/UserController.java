package com.oreillyauto.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.oreillyauto.domain.User;
import com.oreillyauto.model.Message;
import com.oreillyauto.model.Oreilly;
import com.oreillyauto.model.Response;
import com.oreillyauto.service.UserRolesService;
import com.oreillyauto.service.UserService;
import com.oreillyauto.util.Helper;

@Controller
public class UserController extends BaseController {

	@Autowired
	UserService userService;
	
	@Autowired
	UserRolesService userRolesService;
	
	@GetMapping(value = "/users/page1")
	public String getPageOne(HttpResponse response, HttpServletRequest request, HttpSession session, RedirectAttributes redirectAttrs) {
		request.getSession().setAttribute("mySessionMessage", "Hello from session.");
		redirectAttrs.addAttribute("myModelMessage", "Hello from model.");
		return "redirect:/users/page2";
	}
	
	@GetMapping(value = "/users/page2")
	public String getPageTwo(@ModelAttribute("myModelMessage") String myModelMessage) {
		return "page2";
	}
	
	@GetMapping(value = "/users/page3")
	public String getPageThree(HttpSession session, Model model, String id) throws Exception {
		return "page3";
	}
	
	@GetMapping(value = "/users")
	public String getUsers(Model model, String id) throws Exception {
		
		if (Helper.isInteger(id)) {
			return "redirect:/users/userPage?id=" + id;
		}
		
		List<User> userList = userService.getUsers();
		model.addAttribute("userListJson", Helper.getJson(userList));
		return "users";
	}
	
	@GetMapping(value = "/users/addUser")
	public String getAddUser(Model model) {
		model.addAttribute("roles", userRolesService.getUserRoles());
		return "userPage";
	}
	
	@GetMapping(value = "/users/userPage")
	public String getUserPage(Model model, String id) throws Exception {
		Integer idInt = null;
		
		if (Helper.isInteger(id)) {
			idInt = Integer.parseInt(id);			
		} else {
			setMessageOnModel(model, "Invalid User ID", Oreilly.DANGER);
			return "redirect:/users";
		}
		
		User user = userService.getUserById(idInt);
		
		if (user == null) {
			setMessageOnModel(model, "Sorry, user with ID " + id + " not found.", Oreilly.DANGER);
			return "redirect:/users";
		}
		
		model.addAttribute("user", user);
		return "userPage";
	}
	
	@GetMapping(value = "/users/two")
	public String getUsersTwo(Model model) throws Exception {
		return "usersTwo";
	}
	
	@ResponseBody
	@GetMapping(value = "/users/getUserData2")
	public String getUserData(Model model) throws Exception {
		String json = Helper.getJson(userService.getUsers());
		System.out.println("Returning user table in json format:\n" + json);
		return new JsonParser().parse(json).toString();
	}
	
	@ResponseBody
	@GetMapping(value = "/users/getUserData")
	public List<User> getUserData2(Model model) throws Exception {
		return userService.getUsers();
	}
	
	@PostMapping(value = "/users/userPage")
	public String postUserPage(Model model, User user) throws Exception {
		userService.saveUser(user);
		setMessageOnModel(model, "User Updated Successfully", Oreilly.SUCCESS);
		return "redirect:/users";
	}
	
	@ResponseBody
    @PostMapping(value = { "/users/deleteUser" })
    public Response postDeleteUser(@RequestBody User user) {
		Response response = new Response();
		
		try {
	    	if (user != null && user.getUserId() != null) {
	    		System.out.println("Deleting user with ID=" + user.getUserId());
	    		userService.deleteUser(user.getUserId());
	    		response.setMessage("User Successfully Deleted");
		    	response.setMessageType(Oreilly.SUCCESS);
		    	response.setUserList(userService.getUsers());
	    	} else {
	    		response.setMessage("Sorry, unable to find that user.");
		    	response.setMessageType(Oreilly.DANGER);	
	    	}
	    	
	    	return response;
		} catch (Exception e) {
			response.setMessage(Helper.getError(e));
	    	response.setMessageType(Oreilly.DANGER);
	    	return response;
		}
    } 
	
    @ResponseBody
    @PostMapping(value = { "/users/getWelcomeMessage" })
    public String getWelcomeMessage(@RequestBody Message message) throws Exception {
    	
    	if (message != null && message.getMessage() != null && message.getMessage().length() > 0) {
    		System.out.println("Message from page: " + message.getMessage());
    	}
    	
    	return new Gson().toJson(new Message("Welcome to the O'Reilly User Manager"));
    } 
    
}
