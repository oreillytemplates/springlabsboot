package com.oreillyauto.controllers;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oreillyauto.domain.carparts.Carpart;
import com.oreillyauto.dto.Email;
import com.oreillyauto.service.CarpartsService;
import com.oreillyauto.util.Helper;

@Controller
public class CarpartsController {

	@Autowired
	CarpartsService carpartsService;

	// Feedback "Regular" Spring MVC GET Request 
	@GetMapping(value = "/carparts/regular")
	public String getRegularForm(Model model) {
	    Email email = new Email();
	    model.addAttribute("command", email);
	    model.addAttribute("active", "regular");
	    return "springForm";
	}

    // Feedback "Regular" Spring MVC POST Request
    @PostMapping(value = "/carparts/feedback")
    public String postRegularForm(@ModelAttribute("command") Email email, BindingResult result, Model model) {
        System.out.println("from=>" + email.getEmailAddress() + " body=>" + email.getEmailBody());
        model.addAttribute("messageType", "success");
        model.addAttribute("message", "Success! Thank you for your feedback!");
        model.addAttribute("command", new Email());
        
        // Prepare the form again and then load the view
        model.addAttribute("command", email);
	    model.addAttribute("active", "regular");
        return "springForm";
    }

	@GetMapping(value = { "/carparts/emailus" })
	public String getEmailUs(Model model) {
		model.addAttribute("active", "emailus");
		return "emailus";
	}
	
    @ResponseBody
    @PostMapping(value = { "/carparts/emailus" })
    public String postEmailUs(Model model, @RequestBody Email email) {
        try {
            carpartsService.sendEmail(email);
            email.setMessageType("success");
            email.setMessage("Email Sent Successfully!");
            return Helper.getJson(email);
        } catch (Exception e) {
            try {
            	String error = ((e == null || e.getMessage() == null) ? "Null Pointer Exception" : e.getMessage());
            	email.setMessageType("danger");
            	// Do not send e.getMessage() to users in production
                email.setMessage(error);
                return Helper.getJson(email);
            } catch (JsonProcessingException jpe) {
                return "{\"message\":\""+jpe.getMessage()+"\",\"messageType\":\"danger\"}";
            }
        }
    }
    
	@GetMapping(value = { "/carparts/contactus" })
	public String getContactUs(Model model) {
		model.addAttribute("active", "contactus");
		return "contactus";
	}
	
    @PostMapping(value = {"/carparts/contactus"})
    public String postContactUs(Model model, Email email) throws Exception {
        //System.out.println("from=>" + email.getEmailAddress() + " body=>" + email.getEmailBody());
        
        try {
        	carpartsService.sendEmail(email);
        	model.addAttribute("messageType", "success");
        	model.addAttribute("message", "Your message was sent successfully.");
        } catch (Exception e) {
        	model.addAttribute("messageType", "danger");
        	
        	// DO NOT SHOW USERS e.getMessage() IN PRODUCTION
        	model.addAttribute("message", e.getMessage());
        }
        
        return "contactus";
    }

    @GetMapping(value = { "/carparts/partnumber" })
    public String getPartnumber(HttpSession session, Model model, String partnumber, 
       @ModelAttribute("message") String message, 
       @ModelAttribute("messageType") String messageType,
       @ModelAttribute("carpart") Carpart carpart) throws Exception {
    	
        if (partnumber != null && partnumber.length() > 0) {
            Carpart getCarpart = carpartsService.getCarpartByPartNumber(partnumber);
            model.addAttribute("carpart", getCarpart);
        }

        session.setAttribute("key", "value");
        session.removeAttribute("key");
    	model.addAttribute("active", "add");
        return "partnumber";
    }
    
	@PostMapping(value = { "/carparts/partnumber" })
	public String postPartnumber(Model model, String partnumber, String title, String update
	        ,RedirectAttributes ra) {
	    System.out.println("partnumber=>" + partnumber + " title=>" + title);
	    Carpart carpart = carpartsService.addUpdateCarpart(partnumber, title);
	    
        if (carpart != null) {
            String action = ("true".equalsIgnoreCase(update)) ? "Updated" : "Saved";
            ra.addFlashAttribute("message", "Part Number " + partnumber + " "+action+" Successfully!");
            ra.addFlashAttribute("messageType", "success");
            //ra.addAttribute("message", "Part Number " + partnumber + " Saved Successfully!");
            ra.addFlashAttribute("carpart", carpart);
        } else {
            ra.addFlashAttribute("messageType", "danger");
            ra.addFlashAttribute("message", "Part Number " + partnumber + " Not Saved Successfully!");
        }
   
	    return "redirect:/carparts/partnumber";
	}

	@GetMapping(value = { "/carparts/delete/{partnumber}" })
	public String deleteCarpart(@PathVariable String partnumber, Model model) throws Exception {
	        
	    if (partnumber != null && partnumber.length() > 0) {
	        Carpart carpart = carpartsService.getCarpartByPartNumber(partnumber);
	        carpartsService.deleteCarpartByPartNumber(carpart);
	        model.addAttribute("messageType", "success");
	        model.addAttribute("message", "Car Part with part # = " + partnumber + " Deleted Successfully");
	    }
	    
	    return "partnumber";
	}

	@GetMapping(value = { "/carparts/electrical/{partNumber}", "/carparts/engine/{partNumber}", "/carparts/other/{partNumber}" })
	public String getCarpart(@PathVariable String partNumber, Model model, String userId) throws Exception {
		String error = "";
		Carpart carpart = carpartsService.getCarpartByPartNumber(partNumber);
		
		if (carpart == null) {
			error = "Sorry, cannot find part number " + partNumber;
		}
			
		model.addAttribute("carpart", ((carpart == null) ? new Carpart() : carpart));
		model.addAttribute("active", partNumber);
		model.addAttribute("error", error);
		return "carparts";
	}

	@GetMapping(value = { "/carparts/carpartManager" })
	public String getCarpartManager(Model model) {
		List<Carpart> carpartList = carpartsService.getCarparts();
		model.addAttribute("carpartList", carpartList);
		return "carpartManager";
	}

	@GetMapping(value = { "/carparts" })
	public String carparts(Model model, String firstName, String lastName) throws Exception {
		String fullName = ((firstName != null && firstName.length() > 0) ? firstName : "");
		fullName = ((fullName.length() > 0) ? (firstName + " " + lastName) : "");
		model.addAttribute("fullName", fullName);

		// Convert a List into JSON
		final List<Carpart> carpartList = new ArrayList<Carpart>();
		carpartList.add(new Carpart("p1", "l1", "t1", "d1"));
		carpartList.add(new Carpart("p2", "l2", "t2", "d2"));
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(out, carpartList);
		final byte[] data = out.toByteArray();
		// System.out.println(new String(data));

		model.addAttribute("carpartList", carpartList);
		model.addAttribute("carpartListJson", new String(data));
 
		return "carparts";
	}

	@GetMapping(value = { "/carparts/labtwo" })
	public String getLabTwo(Model model) {
		List<Carpart> carpartList = carpartsService.getCarparts();
		model.addAttribute("carpartList", carpartList);
		return "labtwo";
	}

}
