package com.oreillyauto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.oreillyauto.dao.ExampleRepository;
import com.oreillyauto.dao.FacilityRepository;
import com.oreillyauto.dao.InternRepository;
import com.oreillyauto.dao.OrderItemRepository;
import com.oreillyauto.dao.OrderRepository;
import com.oreillyauto.dao.PlannerRepository;
import com.oreillyauto.dao.ProjectRepository;
import com.oreillyauto.dao.SchoolRepository;
import com.oreillyauto.dao.StudentRepository;
import com.oreillyauto.dao.TeammemberRepository;
import com.oreillyauto.domain.facilities.Facility;
import com.oreillyauto.domain.facilities.Teammember;
import com.oreillyauto.domain.interns.Badge;
import com.oreillyauto.domain.interns.Intern;
import com.oreillyauto.domain.orders.Order;
import com.oreillyauto.domain.orders.OrderItem;
import com.oreillyauto.domain.projects.Planner;
import com.oreillyauto.domain.projects.Project;
import com.oreillyauto.domain.schools.School;
import com.oreillyauto.domain.schools.Student;
import com.oreillyauto.service.MappingsService;

@Service("mappingsService")
public class MappingsServiceImpl implements MappingsService {
    @Autowired
    OrderRepository orderRepo;
    @Autowired
    OrderItemRepository orderItemRepo;
    @Autowired
    SchoolRepository schoolRepo;
    @Autowired
    StudentRepository studentRepo;
    @Autowired
    FacilityRepository facilityRepo;
    @Autowired
    TeammemberRepository teammemberRepo;
    @Autowired
    ExampleRepository exampleRepo;
    @Autowired
    ProjectRepository projectRepo;
    @Autowired
    PlannerRepository plannerRepo;
    @Autowired
    InternRepository internRepo;
    
	@Override
	public List<Order> getOrders() {
		return (List<Order>)orderRepo.findAll();
	}

	@Override
	public List<OrderItem> getOrderItems() {
		return (List<OrderItem>)orderItemRepo.findAll();
	}

	@Override
	public List<School> getSchools() {
		return (List<School>)schoolRepo.findAll();
	}

	@Override
	public List<Student> getStudents() {
		return (List<Student>)studentRepo.findAll();
	}

	@Override
	public List<Facility> getFacilities() {
		return (List<Facility>)facilityRepo.findAll();
	}

	@Override
	public List<Teammember> getTeammembers() {
		return (List<Teammember>)teammemberRepo.findAll();
	}

	@Override
	public List<Object> getUserTables() {
//		return (List<Object>)exampleRepo.getTables();
		return null;
	}

	@Override
	public void addUserTablesToTheModel(Model model) {
		List<Object> userTableList = getUserTables();
		model.addAttribute("userTableList", userTableList);
	}

	@Override
	public List<Planner> getPlanners() {
		return (List<Planner>)plannerRepo.findAll();
	}

	@Override
	public List<Project> getProjects() {
		return (List<Project>)projectRepo.findAll();
	}

	@Override
	public List<Intern> getInterns() {
		return (List<Intern>)internRepo.findAll();
	}

	@Override
	public void saveIntern(Intern intern) {
		intern.setBadge(new Badge(intern.getIssuer(), intern));
		internRepo.save(intern);
	}
    
}
