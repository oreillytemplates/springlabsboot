package com.oreillyauto.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.CarpartsRepository;
import com.oreillyauto.domain.carparts.Carpart;
import com.oreillyauto.dto.Email;
import com.oreillyauto.service.CarpartsService;

@Service("carpartsService")
public class CarpartServiceImpl implements CarpartsService {
	@Autowired
	CarpartsRepository carpartsRepo;
	
	@Override
	public Carpart saveCarpart(Carpart carpart) {
		return carpartsRepo.save(carpart);
	}
	
    @Override
    @Transactional
    public Carpart getCarpartByPartNumber(String partNumber) throws Exception {    	
        // Old way (fake it 'til you make it)
    	//return carpartsRepo.getCarpartByPartNumber(partNumber);
    	
    	// Querydsl
    	//return carpartsRepo.getCarpart(partNumber);
    	
    	// CRUD REPO API
//        Optional<Carpart> o = carpartsRepo.findById(partNumber);
//        
//        if (o.isPresent()) {
//            return o.get();
//        } else {
//            return null;
//        }
    	
    	// SPRING DATA API
    	return carpartsRepo.findByPartNumber(partNumber);
    }

	@Override
	public List<Carpart> getCarparts() {
		//return carpartsRepo.getCarparts();
		
		// CRUD REPO API
		return (List<Carpart>)carpartsRepo.findAll();
	}

	@Override
	public void deleteCarpartByPartNumber(Carpart carpart) {
	    carpartsRepo.delete(carpart);
	}

    @Override
    public void sendEmail(Email email) throws Exception {
        Properties appProperties = new Properties();
        appProperties.load(CarpartServiceImpl.class.getClassLoader().getResourceAsStream("email.properties"));
        final String authEmailAddress = appProperties.getProperty("gmail.username");
        final String authPassword = appProperties.getProperty("gmail.password");
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(authEmailAddress, authPassword);
            }
        });

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(email.getEmailAddress()));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(authEmailAddress));
        message.setSubject("Contact Us Submission");
        message.setText(email.getEmailBody());
        Transport.send(message);
    }

    @Override
    public Carpart addUpdateCarpart(String partnumber, String title) {
        Carpart carpart = new Carpart();
        carpart.setPartNumber(partnumber);
        carpart.setTitle(title);
        carpart.setLine("");
        carpart.setDescription("");        
        return saveCarpart(carpart);
    }

}
