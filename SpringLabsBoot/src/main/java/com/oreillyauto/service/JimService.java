package com.oreillyauto.service;

import java.util.List;
import java.util.Map;

import com.oreillyauto.domain.Course;
import com.oreillyauto.domain.University;

public interface JimService {
    public Map<String, Course> getDetails();
    public Map<String, List<University>> getReport();

}
