package com.oreillyauto.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(final WebSecurity web) {
		web.ignoring().antMatchers("/", "/resources/**", "/index/**", "/login/", 
				"/example/**", "/carparts/**", "/users/**", "/employees/**",
				"/fetch/**", "/mappings/**", "/foo/**", "/rest/**", "/jim/**",
				"/favicon.ico");
    }
    
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf()
                .disable()
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .usernameParameter("username")
                .passwordParameter("password")
                .loginPage("/login/")
                .loginProcessingUrl("/loginProcess")
                .defaultSuccessUrl("/home", true)
                .and()
                .exceptionHandling()
                .accessDeniedPage("/403")
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/logoutSuccess");
        ;
    }

    @Bean
    public UserDetailsService userDetailsService() {
        User.UserBuilder users = User.withDefaultPasswordEncoder();
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(users.username("user").password("password").roles("USER").build());
        manager.createUser(users.username("admin").password("password").roles("USER", "ADMIN").build());
        return manager;
    }

}




//package com.oreillyauto.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true) // need this to use method-level security
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//
//    @Override
//    public void configure(final WebSecurity web) {
//		web.ignoring().antMatchers("/resources/**", "/carparts/**", "/login/**"); // TODO: Figure out which resources this is
//    }
//
//    @Override
//    protected void configure(final HttpSecurity http) throws Exception {
//        http.csrf()
//        	.disable()
//        	.formLogin()
//        		.usernameParameter("username")
//                .passwordParameter("password")
//                .loginPage("/login/")
//                .loginProcessingUrl("/loginProcess")
//                .defaultSuccessUrl("/home", true)
//        .and()
//        	.exceptionHandling()
//        		.accessDeniedPage("/403")
//        .and()
//        	.logout()
//                .logoutUrl("/logout")
//                .logoutSuccessUrl("/logoutSuccess")
//        ;
//    }
//
//    /**
//     * Why {noop}? 
//     * https://mkyong.com/spring-boot/spring-security-there-is-no-passwordencoder-mapped-for-the-id-null/
//     * @param auth
//     * @throws Exception
//     */
//    @Autowired
//    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("user").password("{noop}user").roles("USER");
//        auth.inMemoryAuthentication().withUser("admin").password("{noop}admin").roles("ADMIN");
//    }
//
//}
